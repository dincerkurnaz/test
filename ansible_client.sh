#!/bin/bash

# signalive kullanicisini sudoers e ekliyoruz.
sudo ansible all -i "localhost," -c local -m lineinfile -a "dest=/etc/sudoers state=present line='signalive ALL=(ALL) NOPASSWD: ALL'"
# ansible host dosyasini ayarliyoruz.
sudo ansible all -i "localhost," -c local -m lineinfile -a "dest=/etc/ansible/hosts state=present line='localhost ansible_connection=local'"

# ansbile klasorunun hakkini root dan aliyoruz.
sudo chown -R signalive .ansible/

#git valid olmayan sertifika icin ayar
git config --global http.sslverify false

#her saat calismasi icin crontab her saati 1dk gece calisacak.
ansible all -i "localhost," -c local -m cron -a 'name="installer" minute="45" hour="*" job="/usr/bin/ansible-pull -U https://dincerkurnaz@bitbucket.org/dincerkurnaz/test.git/ -d /home/signalive/.installer --full -o installer.yml >> /tmp/ansible-pull.log 2>&1"'
ansible all -i "localhost," -c local -m cron -a 'name="command" minute="15" hour="*" job="/usr/bin/ansible-pull -U https://dincerkurnaz@bitbucket.org/dincerkurnaz/ansible-agent.git/ -d /home/signalive/.command --full -o command.yml >> /tmp/ansible-pull.log 2>&1"'
exit
